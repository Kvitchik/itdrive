/*
Task: Реализовать функцию, которая выводит наиболее часто встречающиеся буквы в тексте (первую тройку),
     если количество букв совпадает - вывести первую тройку в алфавитном порядке.
*/

import java.util.Arrays;

class Main {
    public static void main(String[] args) {

        // анализируемый текст
        String text = "Hello! My name is Rodion! Привет! Меня зовут Родион!";

        // найдем самый "дальний" элемент в таблице UTF-16
        int maxElem = Integer.MIN_VALUE; 
        for (char c : text.toCharArray()) { 
            if (c > maxElem) {
                maxElem = c;
            }
        }
        System.out.println("Max Elem: " + (char)maxElem + ". Index: " + maxElem);

        // создаем массив, где индекс - кодовое представление символа, значение - сколько раз этот символ встретился
        int[] mas = new int[maxElem+1];
        for (char c : text.toCharArray()) {
            mas[c]++;
        }
        System.out.println(Arrays.toString(mas));

        // найдем 3 наиболее часто встречающихся символа
        int firstMaxValue = 0;
        int firstMaxIndex = 0;
        int secondMaxValue = 0;
        int secondMaxIndex = 0;
        int thirdMaxValue = 0;
        int thirdMaxIndex = 0;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] > firstMaxValue) {
                thirdMaxValue = secondMaxValue;
                thirdMaxIndex = secondMaxIndex;
                secondMaxValue = firstMaxValue;
                secondMaxIndex = firstMaxIndex;
                firstMaxValue = mas[i];
                firstMaxIndex = i;
            } else if (mas[i] > secondMaxValue) {
                thirdMaxValue = secondMaxValue;
                thirdMaxIndex = secondMaxIndex;
                secondMaxValue = mas[i];
                secondMaxIndex = i;
            } else if (mas[i] > thirdMaxValue) {
                thirdMaxValue = mas[i];
                thirdMaxIndex = i;
            }
        }

        System.out.println("Char: " + (char)(firstMaxIndex) + ". Count: " + firstMaxValue);
        System.out.println("Char: " + (char)(secondMaxIndex) + ". Count: " + secondMaxValue);
        System.out.println("Char: " + (char)(thirdMaxIndex) + ". Count: " + thirdMaxValue);
    }

}