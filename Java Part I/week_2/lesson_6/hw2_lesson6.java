/*
Task: Реализовать функцию, которая принимает на вход массив символов-цифр,
например: {'2','3','1','1'}, а возвращает тип int -> 2311. (Преобразование строки в число).
Строка - массив символов.
*/

class Main {
    public static void main(String[] args) {
        System.out.println(convertToInteger('2','3','1','1'));
    }

    private static int convertToInteger(char ... array) {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            if (isDigit(array[i])) {
                result = result + array[i] - '0';
                if (i+1 != array.length)
                    result *= 10;
            }
        }
        return result;
    }

    private static boolean isDigit(char c) {
        if (c > '0' && c < '9')
            return true;
        return false;
    }
}