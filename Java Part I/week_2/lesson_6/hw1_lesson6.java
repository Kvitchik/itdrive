/*
Task: Реализовать функцию сравнения строк (описано в уроке).
*/

public class Main {
    public static void main(String[] args) {
        char[] mas1 = {'a', 'a', 'a'};
        char[] mas2 = {'a', 'a'};
        char[] mas3 = {'a', 'b', 'c'};
        System.out.println(stringsCompare(mas1, mas2));
    }

    private static int stringsCompare(char[] a, char[] b) {
        int length = a.length < b.length ? a.length : b.length; // в цикле будем использовать длину наименьшего массива

        int result = 0; // делаем на случай, если длины массивов разные
        if (a.length < b.length) {
            result = -1;
        } else if (a.length > b.length) {
            result = 1;
        } else {
            result = 0;
        }

        for (int i = 0; i < length; i++) {
            if (a[i] < b[i]) {
                return -1;
            } else if (a[i] > b[i]) {
                return 1;
            }
        }
        return result;
    }
}
