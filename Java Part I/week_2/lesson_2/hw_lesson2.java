/*
Task: Реализовать функцию для вычисления определенного интеграла методом Симпсона.
*/ 

class Main {
    public static void main(String[] args) {
        double a = 0;
        double b = 4;
        int n = 1;
        double result = integral(a,b,n);
        System.out.println(result);
    }

    private static double f(double x) {
        return x * x;
    }

    private static double integral(double a, double b, int n) {
        double h = (b - a) / (2 * n);
        double result = 0;
        for (double x = a + h; x < b; x = x + h) {
            double currentRectangle = (h/3) * (f(x-h) + 4*f(x) + f(x+h));
            result = result + currentRectangle;
        }
        return result;
    }
}