/*
Task: Написать функцию, возвращающую сумму цифр определенного числа.
*/ 

class Main {
    public static void main(String[] args) {
        System.out.println(sum(123));
    }

    private static int sum(int number) {
        int sum = 0;
        while(number > 9) {
            sum = sum + (number % 10);
            number = number / 10;
        }
        sum = sum + number;
        return sum;
    }
}