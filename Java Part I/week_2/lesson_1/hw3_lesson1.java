/*
Task: Написать функцию, которая возвращает сумму чисел, которые находятся в диапазоне от a до b.
*/ 

class Main {
    public static void main(String[] args) {
        System.out.println(sum(2,4));
    }

    private static int sum(int a, int b) {
        int sum = 0;
        for (int i = a; i <= b; i++) {
            sum = sum + i;
        }
        return sum;
    }
}