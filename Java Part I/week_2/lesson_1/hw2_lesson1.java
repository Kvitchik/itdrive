/*
Task: Написать функцию, возвращающую зеркальное представление числа.
*/ 

class Main {
    public static void main(String[] args) {
        System.out.println(miracle(12345));
    }

    private static int miracle(int number) {
        int res = 0;
        while(number > 9) {
            res = res + (number % 10);
            number = number / 10;
            res = res * 10;
        }
        res = res + number;
        return res;
    }
}