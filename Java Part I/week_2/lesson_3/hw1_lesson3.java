/*
Task: Реализовать процедуру сортировки выбором.
*/

import java.util.Arrays;

class Test {
    public static void main(String[] args) {
        int[] array = {9,1,8,2,7,3,6,5,0,4,8,123,76,12,-5};
        System.out.println(Arrays.toString(array));
        sort(array);
    }

    private static void sort(int ... mas) {
        int index = 0;
        int tmp = 0;
        int j = 0;
        int i = 0;
        for (i = 0; i < mas.length; i++) {
            index = i;
            for (j = i + 1; j < mas.length; j++) {
                if (mas[index] > mas[j]) {
                    index = j; // сохраняем индекс минимального элемента подмассива
                }
            }
            tmp = mas[index];
            mas[index] = mas[i];
            mas[i] = tmp;
        }
        System.out.println(Arrays.toString(mas));
    }
}