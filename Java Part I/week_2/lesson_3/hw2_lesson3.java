/*
Task: Реализовать функцию бинарного поиска через цикл.
*/

import java.util.Arrays;

class Main {
    public static void main(String[] args) {
        // пусть массив будет изначально не отсортирован
        int[] array = {1,7,2,9,4,8,5,6,77,4,5346,21};
        System.out.println(binarySearch(array,900000));
    }

    // будем возвращать индекс найденного элемента или -1, если такого нет
    private static int binarySearch(int[] array, int value) {
        array = sort(array); 
        int leftBound = 0;
        int rightBound = array.length;
        int middle = (rightBound + leftBound) >> 1; // спасаемся от переполнения

        while(true) {
            if (array[middle] == value) {
                return middle;
            }
            if (middle == 0 || value < array[leftBound] || value > array[rightBound-1]) {
                return -1;
            }

            if (value < array[middle]) {
                rightBound = middle;
            } else {
                leftBound = middle;                
            }
            middle = (rightBound + leftBound) >> 1 ; 
        }
    }

    private static int[] sort(int ... mas) {
        int index = 0;
        int tmp = 0;
        int j = 0;
        int i = 0;
        for (i = 0; i < mas.length; i++) {
            index = i;
            for (j = i + 1; j < mas.length; j++) {
                if (mas[index] > mas[j]) {
                    index = j; // сохраняем индекс минимального элемента подмассива
                }
            }
            tmp = mas[index];
            mas[index] = mas[i];
            mas[i] = tmp;
        }
        System.out.println(Arrays.toString(mas));
        return mas;
    }
}