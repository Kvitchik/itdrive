/*
Task: Написать функцию pow(double x, int y) - она возводит число x в произвольную степень y.
      Написать две функции для расчета интеграла методом Симпсона и методом прямоугольников.
*/

class Main {
    public static void main(String[] args) {
        int ns[] = {100, 1000, 10000, 100000, 1000000}; // разбиения
        int ys[] = {2, 3, 4, 5, 6, 7}; // степени
        for (int i = 0; i < ns.length; i++) {
            for (int j = 0; j < ys.length; j++) {
                System.out.println("Simpson - " + ns[i] + " - x^" + ys[j] + " = " + integralSimpson(0, 4, ns[i], ys[j]));
                System.out.println("Rectangle - " + ns[i] + " - x^" + ys[j] + " = " + integralRectangle(0, 4, ns[i], ys[j]));
            }
        }
    }

    private static double pow(double x, double y) {
        return Math.pow(x, y);
    }

    private static double f(double x, double y) {
        return pow(x, y);
    }

    private static double integralSimpson(double a, double b, int n, int y) {
        double h = (b - a) / n;
        double result = 0;
        int index = 0;
        for (double i = a; i <= b; i = i + h) {
            if (index == 0 || index == n)
                result += f(i, y);
            else if (index % 2 != 0)
                result += 4 * f(i, y);
            else
                result += 2 * f(i, y);
            index++;
        }
        result = result * (h / 3);
        return result;
    }

    private static double integralRectangle(double a, double b, int n, int y) {
        double h = (b - a) / n;
        double result = 0;
        for (double x = a; x <= b; x = x + h) {
            double currentRectangle = f(x, y) * h;
            result = result + currentRectangle;
        }
        return result;
    }
}
