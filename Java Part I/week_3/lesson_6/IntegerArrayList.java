// TODO: Реализовать в IntegersArrayList и IntegersLinkedList следующие методы:
//  DONE 1. void add(int element, int index) - добавление элемента в произвольную позицию списка. Учитывать,
//     что index не должен быть больше, чем фактическое количество элементов в списке.
//  DONE 2. void remove(int index) - удаление элемента из списка по его порядковому номеру.
//  DONE 3. void reverse() - разворот списка.
//  DONE 4. Для списка на основе массива предусмотреть ситуацию переполнения списка. В случае,
//  когда количество элементов массива равно ему фактическому размеру - увеличить размер массива в полтора раза.

import java.util.Arrays;

public class IntegerArrayList {
    private int capacity = 10;
    private int[] array;
    private int count;

    IntegerArrayList() {
        array = new int[capacity];
        count = 0;
    }

    public void add(int element) {
        if (count < capacity) {
            array[count] = element;
            count++;
        } else {
            capacity *= 1.5;
            array = Arrays.copyOf(array, capacity);
            array[count] = element;
            count++;
        }
    }

    public void add(int element, int index) {
        if (count < capacity) {
            if (index >= 0 && index < count) {
                for (int i = count; i >= index; i--) {
                    array[i] = array[i - 1];
                }
                array[index] = element;
                count++;
            } else {
                System.out.println("Ошибка при добавлении элемента! Такого индекса в массиве не существует!");
            }
        } else {
            capacity *= 1.5;
            array = Arrays.copyOf(array, capacity);
            add(element,index);
        }
    }

    public void remove(int index) {
        if (index >= 0 && index < count) {
            int[] newArray = new int[capacity];
            // копируем массив ДО нужного индекса
            for (int i = 0; i < index; i++) {
                newArray[i] = array[i];
            }
            // копируем остальную часть массива (затирая удаляемый элемент)
            for (int i = index; i < array.length - 1; i++) {
                newArray[i] = array[i + 1];
            }
            array = newArray;
            count--;
        } else {
            System.out.println("Ошибка при удалении элемента! Такого индекса в массиве не существует!");
        }
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return array[index];
        } else {
            System.out.println("Ошибка при получении элемента! Такого индекса в массиве не существует!");
            return -1;
        }
    }

    public int getCount() {
        return count;
    }

    public void reverse() {
        int tmp;
        for (int i = 0; i < count / 2; i++) {
            tmp = array[i];
            array[i] = array[count - 1 - i];
            array[count - 1 - i] = tmp;
        }
    }
}
