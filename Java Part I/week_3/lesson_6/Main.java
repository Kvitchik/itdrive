public class Main {

    public static void main(String[] args) {
        testArrayList();
//        testLinkedList();
//        testLinkedListIterator();

    }

    private static void testLinkedListIterator() {
        IntegerLinkedList list = new IntegerLinkedList();
        list.add(1);
        list.add(2);
        list.add(3);

        IntegerLinkedList.LinkedListIterator iterator = list.new LinkedListIterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    private static void testLinkedList() {
        IntegerLinkedList list = new IntegerLinkedList();
        // добавим 5 элементов
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }

        // выведем список на экран
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }

        // добавим в конец
        list.add(5);
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }

        // добавим в произвольное место
        list.add(10000,0);
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }

        list.add(-10000,list.getCount()-1);
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }

        list.add(5000,2);
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }


        // удаляем
        list.remove(0);
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }

        list.remove(1);
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }

        list.remove(5);
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }


        // перевернем список
        list.reverse();
        System.out.println();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }

    private static void testArrayList() {
        IntegerArrayList list = new IntegerArrayList();
        // добавим 5 элементов
        for (int i = 0; i < 18; i++) {
            list.add(i);
        }
        // выведем на экран
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();

        // добавление в произвольное место
        list.add(1000, 5);
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();

        // удаление
        list.remove(5);
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();

        // переворачиваем
        list.reverse();
        for (int i = 0; i < list.getCount(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }
}
