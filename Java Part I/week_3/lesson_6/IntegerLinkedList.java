// TODO: Реализовать в IntegersArrayList и IntegersLinkedList следующие методы:
//  DONE | 1. void add(int element, int index) - добавление элемента в произвольную позицию списка. Учитывать,
//     что index не должен быть больше, чем фактическое количество элементов в списке.
//  DONE || 2. void remove(int index) - удаление элемента из списка по его порядковому номеру.
//  DONE 3. void reverse() - разворот списка.
//  DONE 4. Реализовать метод int get(int index).

public class IntegerLinkedList {
    private Node first;
    private Node last;
    private int count;

    private static class Node {
        private int value;
        private Node next;

        Node(int value) {
            this.value = value;
            next = null;
        }
    }

    class LinkedListIterator {
        Node currentNode;
        public LinkedListIterator() {
            this.currentNode = first;
        }

        boolean hasNext() {
            return currentNode != null;
        }

        int next() {
            Node temp = currentNode;
            currentNode = currentNode.next;
            return temp.value;
        }
    }

    public IntegerLinkedList() {
        first = null;
        last = null;
        count = 0;
    }

    public void add(int element, int index) {
        Node newNode = new Node(element);
        if (index < count) {
            if (index == 0) {
                newNode.next = first;
                first = newNode;
            } else {
                int counter = 0;
                Node prev = null;
                Node currentNode = first;
                while (counter != index) {
                    prev = currentNode;
                    currentNode = currentNode.next;
                    counter++;
                }
                prev.next = newNode;
                newNode.next = currentNode;
            }
            count++;
        } else {
            System.out.println("Ошибка при произвольном добавлении! Такого индекса не существует");
        }
    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first != null) {
            last.next = newNode;
            last = newNode;
        } else {
            first = newNode;
            last = newNode;
        }
        count++;
    }

    public int get(int index) {
        if (index < count) {
            int counter = 0;
            Node currentNode = first;
            while (counter != index) {
                currentNode = currentNode.next;
                counter++;
            }
            return currentNode.value;
        } else {
            System.out.println("Ошибка при получении элемента! Такого индекса не существует!");
            return -1;
        }
    }

    public int getCount() {
        return count;
    }

    public void remove(int index) {
        if (index < count && index > 0) {
            int counter = 0;
            Node prev = null;
            Node currentNode = first;
            while (counter != index) {
                prev = currentNode;
                currentNode = currentNode.next;
                counter++;
            }
            prev.next = currentNode.next;
            count--;
        } else if (index == 0) {
            first = first.next;
            count--;
        } else {
            System.out.println("Ошибка при произвольном добавлении! Такого индекса не существует");
        }
    }

    public void reverse() {
        Node prev = null;
        Node current = first;
        Node next = first;
        while (next != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        first = prev;
    }
}
