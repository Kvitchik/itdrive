/*
Task: Упорядочить массив строк в лексикографическом порядке (см. видеоурок)
*/

import java.util.Arrays;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        final int COUNT_LINES = 4;
        Scanner sc = new Scanner(System.in);
        char[][] lines = new char[COUNT_LINES][];
        for (int i = 0; i < lines.length; i++) {
            lines[i] = sc.nextLine().toCharArray();
        }

        // сортировка
        char[] tmp;
        for (int i = 0; i < lines.length - 1; i++) {
            for (int j = 0; j < lines.length - 1; j++) {
                if (stringsCompare(lines[j], lines[j + 1]) == 1) {
                    tmp = lines[j];
                    lines[j] = lines[j + 1];
                    lines[j + 1] = tmp;
                }
            }

        }
        System.out.println(Arrays.deepToString(lines));
    }

    private static int stringsCompare(char[] a, char[] b) {
        int length = a.length < b.length ? a.length : b.length; // в цикле будем использовать длину наименьшего массива

        int result = 0; // делаем на случай, если длины массивов разные
        if (a.length < b.length) {
            result = -1;
        } else if (a.length > b.length) {
            result = 1;
        } else {
            result = 0;
        }

        for (int i = 0; i < length; i++) {
            if (a[i] < b[i]) {
                return -1;
            } else if (a[i] > b[i]) {
                return 1;
            }
        }
        return result;
    }
}
