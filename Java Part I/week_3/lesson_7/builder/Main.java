/*
Task: Реализовать паттерн Builder().
*/

public class Main {
    public static void main(String[] args) {
        User user = new User.Builder()
                .withName("Rodion")
                .withSurname("kvitchik")
                .withEmail("kvitchik.rodion@gmail.com")
                .build();

        System.out.println(user);
    }
}
