public class User {

    private String firstName;
    private String lastName;
    private int age;
    private String job;
    private String email;

    public static class Builder {
        private User newUser;

        public Builder() {
            newUser = new User();
        }

        public Builder withName(String firstName){
            newUser.firstName = firstName;
            return this;
        }

        public Builder withSurname(String lastName){
            newUser.lastName = lastName;
            return this;
        }

        public Builder withAge(int age){
            newUser.age = age;
            return this;
        }

        public Builder withJob(String job){
            newUser.job = job;
            return this;
        }

        public Builder withEmail(String email){
            newUser.email = email;
            return this;
        }

        public User build(){
            return newUser;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", job='" + job + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
