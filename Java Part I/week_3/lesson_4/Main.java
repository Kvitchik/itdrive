/* Task:
Телевизор - название модели, список каналов (каждый канал имеет свой номер).
Канал - название канала, список передач.
Передача - название передачи.
Пульт - имеет ссылку на телевизор. В классе RemoteController реализовать метод on(int channelNumber),
    который "включит" какой-либо канал, где будет "показана" какая-либо случайная передача.
* */

class Main {
    public static void main(String[] args) {
        TV tvOne = new TV("Samsung 4K",2);
        RemoteController pult = new RemoteController();
        pult.setTV(tvOne);
        tvOne.setRemoteController(pult);

        System.out.println(pult.getShowRightNow());

        Channel channelOne = new Channel("Россия 1");
        Channel channelTwo = new Channel("Первый канал");

        Broadcast broadcastOne = new Broadcast("Новости спорта");
        Broadcast broadcastTwo = new Broadcast("Прогноз погоды");
        Broadcast broadcastThree = new Broadcast("Пусть говорят");
        Broadcast broadcastFour = new Broadcast("Чемпионат мира по футбол");

        channelOne.addBroadcast(broadcastOne);
        channelOne.addBroadcast(broadcastTwo);
        channelTwo.addBroadcast(broadcastThree);
        channelTwo.addBroadcast(broadcastFour);

        tvOne.addChannel(channelOne);
        tvOne.addChannel(channelTwo);

        pult.on(1);
        System.out.println(pult.getShowRightNow());

        pult.on(0);
        System.out.println(pult.getShowRightNow());

        Channel channelThree = new Channel("МАТЧ");
        tvOne.addChannel(channelThree);
    }
}
