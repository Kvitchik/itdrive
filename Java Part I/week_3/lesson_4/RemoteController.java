
// Пульт - имеет ссылку на телевизор. В классе RemoteController реализовать метод on(int channelNumber),
//    который "включит" какой-либо канал, где будет "показана" какая-либо случайная передача.
public class RemoteController {
    private TV tv;

    public void setTV(TV tv) {
        this.tv = tv;
    }

    public void on(int channelNumber) {
        tv.enableChannel(channelNumber);
    }

    // Выводим информацию о том, какой канал и какая передача идет в данный момент
    public String getShowRightNow() {
        return this.tv.getShow();
    }
}
