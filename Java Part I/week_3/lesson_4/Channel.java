import java.util.Random;

// Канал - название канала, список передач.
public class Channel {
    private Random random;
    private char[] name;
    private final int MAX_BROADCASTS = 10;
    private Broadcast[] listBroadcasts;
    private int currentCountBroadcasts;
    // передача, которую в данный момент показываем
    private int indexShowingBroadcast;

    Channel(String name) {
        setName(name);
        this.currentCountBroadcasts = 0;
        listBroadcasts = new Broadcast[MAX_BROADCASTS];
    }

    public void setName(String name) {
        this.name = name.toCharArray();
    }

    // добавим передачу в наш список передач
    public void addBroadcast(Broadcast broadcast) {
        listBroadcasts[currentCountBroadcasts] = broadcast;
        currentCountBroadcasts++;
    }

    // возвращаем название передачи, которая транслируется в данный момент
    public char[] getShowBroadcast() {
        return listBroadcasts[indexShowingBroadcast].getName();
    }

    public void setShowRandomBroadcast() {
        random = new Random();
        indexShowingBroadcast = random.nextInt(currentCountBroadcasts);
    }

    public char[] getName() {
        return this.name;
    }
}
