
// Передача - название передачи.
public class Broadcast {
    private char[] name;

    Broadcast(String name) {
        setName(name);
    }

    public void setName(String name) {
        this.name = name.toCharArray();
    }

    public char[] getName() {
        return this.name;
    }
}
