
import java.util.Arrays;

// Телевизор - название модели, список каналов (каждый канал имеет свой номер).
public class TV {
    private Channel[] listChannels;
    private RemoteController remoteController;
    private char[] name;
    private int currentCountChannels;
    private int MAX_CHANNELS;
    private int indexShowingChannel;

    TV(String name, int MAX_CHANNELS) {
        setName(name);
        this.MAX_CHANNELS = MAX_CHANNELS;
        listChannels = new Channel[this.MAX_CHANNELS];
        currentCountChannels = 0;
    }

    public void addChannel(Channel channel) {
        if (currentCountChannels < MAX_CHANNELS) {
            listChannels[currentCountChannels] = channel;
            currentCountChannels++;
        } else {
            System.err.println("Превышено максимальное количество каналов для телевизора " + String.valueOf(this.name)) ;
        }
    }

    public void setRemoteController(RemoteController remoteController) {
        this.remoteController = remoteController;
    }

    public void setName(String name) {
        this.name = name.toCharArray();
    }

    public void enableChannel(int channelNumber) {
        if (channelNumber <= currentCountChannels) {
            listChannels[channelNumber].setShowRandomBroadcast();
            indexShowingChannel = channelNumber;
        } else {
            System.err.println("Нет такого канала!");
        }
    }

    // что показывают по телевизору?
    public String getShow() {
        if (currentCountChannels != 0) {
            return "Канал: " + String.valueOf(listChannels[indexShowingChannel].getName()) + "\n" +
                    "Передача: " + String.valueOf(listChannels[indexShowingChannel].getShowBroadcast());
        } else {
            return "Ничего не показывает! Добавьте канал!";
        }
    }
}
