/**
 * Task: Создать массив из 100 людей. Каждый человек имеет возраст и имя (массив символов).
 * Заполнить массив случайными данными. Написать программу, которая принимает на вход
 * массив людей и выводит возраст, который встречается чаще всего.
 */

import java.util.Random;

class Main {
    public static void main(String[] args) {
        String[] randomNames = {"Василий", "Евгений","Анатолий", "Иннокентий", "Григорий", "Петр", "Ильнур"};
        Random random = new Random();
        Human[] humans = new Human[100];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].age = random.nextInt(85);
            humans[i].name = randomNames[random.nextInt(randomNames.length)];
        }

        // Вывод на экран
        for (int i = 0; i < humans.length; i++) {
            System.out.print(i + ". " + humans[i].name + " " + humans[i].age);
            System.out.println();
        }

        System.out.println("Самый частый возраст: " + findMostFreqAge(humans));
    }

    private static int findMostFreqAge(Human[] humans) {
        int[] arrayAge = new int[100];
        for (int i = 0; i < humans.length; i++) {
            arrayAge[humans[i].age]++;
        }

        // найдем наиболее часто встречающийся возраст
        int mostFreqAge = 0;
        for (int i = 0; i < arrayAge.length; i++) {
            if (arrayAge[i] > mostFreqAge) {
                mostFreqAge = i;
            }
        }
        return mostFreqAge;
    }
}
