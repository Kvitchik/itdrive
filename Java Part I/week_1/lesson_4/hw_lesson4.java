/*
Task: Реализовать программу, которая выводит произведение чисел бесконечной
последовательности (оканчивающейся на -1 ;), у которых сумма цифр - четное число.
*/ 

import java.util.Scanner;

class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int inputNumber = sc.nextInt();
        int inputNumberCopy = 0;
        int sumDigits = 0; // сумма цифр
        int multipleNumbers = 1; // произведение бесконечной последовательности
        while(inputNumber != -1) {
            inputNumberCopy = inputNumber;
            while(inputNumberCopy > 9) { // делаем цикл, пока число не будет состоять из 1 цифры
                sumDigits = sumDigits + (inputNumberCopy % 10);
                inputNumberCopy = inputNumberCopy / 10;
            }
            sumDigits = sumDigits + inputNumberCopy;
            if (sumDigits % 2 == 0) { // если сумма цифр четная
                System.out.println("-------" + multipleNumbers + " * " + inputNumber);
                multipleNumbers = multipleNumbers * inputNumber;
            }
            inputNumber = sc.nextInt();
            sumDigits = 0;
        }
        System.out.println("Произведение чисел бесконечной последовательсти = " + multipleNumbers);
    }
}