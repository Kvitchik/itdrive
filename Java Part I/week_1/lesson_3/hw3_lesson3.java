/*
Task: Написать программу, которая выведет двоичное представление пятизначного числа
*/ 

class Main {
    public static void main(String[] args) {
        int number = 1546;
        System.out.println(toBinary(number));
    }
    private static StringBuilder toBinary(int number) {
        String result = "";
        while(number != 1) {
            result += number % 2;
            number /= 2;
        }
        result += 1;
        return new StringBuilder(result).reverse();
    }
}