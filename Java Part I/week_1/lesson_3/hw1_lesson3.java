/*
Task: Написать программу, которая выводит сумму цифр пятизначного числа
*/ 

class Main {
    public static void main(String[] args) {
        int number = 123456;
        System.out.println(sumOfDigits(number));
    }

    private static int sumOfDigits(int number) {
        int len = String.valueOf(number).length();
        int result = 0;
        for (int i = 0; i < len; i++) {
            result += number % 10;
            number /= 10;
        }
        return result;
    }
}