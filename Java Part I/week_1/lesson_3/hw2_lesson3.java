/*
Task: Написать программу, которая выводит зеркальное представление пятизначного числа. Например, 34765 в виде 56743
*/ 

class Main {
    public static void main(String[] args) {
        int number = 1;

// вариант с циклом
        int num = number;
        int res = 0;
        while(num > 9) {
            res = res + (num % 10);
            num = num / 10;
            res = res * 10;
        }
        res = res + num;
        System.out.println(res);

// вариант без цикла
        int one = number % 10;
        number = number / 10;
        System.out.println(one + " " + number);

        int two = number % 10;
        number = number / 10;
        System.out.println(two + " " + number);

        int three = number % 10;
        number = number / 10;
        System.out.println(three + " " + number);

        int four = number % 10;
        number = number / 10;
        System.out.println(four + " " + number);

        int five = number;
        System.out.println(five + " " + number);

        int result = 0;
        result = one;
        result = result * 10 + two;
        result = result * 10 + three;
        result = result * 10 + four;
        result = result * 10 + five;
        System.out.println(result);

        // System.out.println(miracle(number));
    }



    // deprecated
    private static int miracle(int number) {
        String s = String.valueOf(number);
        char[] mas = s.toCharArray();
        char tmp;
        for (int i = 0; i < mas.length/2; i++) {
            tmp = mas[i];
            mas[i] = mas[mas.length - i - 1];
            mas[mas.length - i - 1] = tmp;
        }
        return Integer.parseInt(String.valueOf(mas));
    }
}