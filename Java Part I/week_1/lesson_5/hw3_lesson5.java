/*
Task: Выполнить разворот массива (первый элемент становится последним, второй - 
предпоследним и т.д., последний элемент становится первым, предпоследний вторым и т.д.)
*/ 

import java.util.Arrays;

class Main {
    public static void main(String[] args) {
        int[] mas = new int[]{3153,346,346,1,7,6,2,6,2};
        int tmp = 0;
        System.out.println(Arrays.toString(mas));
        for (int i = 0; i < mas.length/2; i++) {
            tmp = mas[i];
            mas[i] = mas[mas.length - i - 1];
            mas[mas.length - i - 1] = tmp;
        }
        System.out.println(Arrays.toString(mas));
    }
}