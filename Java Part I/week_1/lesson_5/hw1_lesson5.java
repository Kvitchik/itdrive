/*
Task: Поменять местами максимальный и минимальный элемент массива.
*/ 

import java.util.Arrays;

class Main {
    public static void main(String[] args) {
        int[] mas = new int[]{2,56,1,6,2,0};
        System.out.println(Arrays.toString(mas));

        int indexMaxElem = 0;
        int indexMinElem = 0;

        int tmp = 0;

        for (int i = 0; i < mas.length; i++) {
            if (mas[i] > mas[indexMaxElem]) {
                indexMaxElem = i;
            } else if (mas[i] < mas[indexMinElem]) {
                indexMinElem = i;
            }
        }

        tmp = mas[indexMaxElem];
        mas[indexMaxElem] = mas[indexMinElem];
        mas[indexMinElem] = tmp;
        System.out.println(Arrays.toString(mas));
    }
}