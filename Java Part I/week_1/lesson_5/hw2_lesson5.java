/*
Task: Вывести среднее арифметическое элементов массива.
*/ 

import java.util.Arrays;

class Main {
    public static void main(String[] args) {
        int[] mas = new int[]{1,2,3,5};

        double sum = 0;
        
        for (int i = 0; i < mas.length; i++) {
            sum = sum + mas[i];
        }

        double mean = sum / mas.length;

        System.out.println(mean);
    }
}