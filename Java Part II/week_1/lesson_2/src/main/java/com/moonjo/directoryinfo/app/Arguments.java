package com.moonjo.directoryinfo.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators="=")
class Arguments {

    @Parameter(names = {"--dirName"})
    public String dirName;
}
