package com.moonjo.directoryinfo.app;

import com.beust.jcommander.JCommander;
import com.moonjo.directoryinfo.utils.FileInformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws FileNotFoundException {
        Arguments arguments = new Arguments();

        JCommander.newBuilder()
            .addObject(arguments)
            .build()
            .parse(args);

        FileInformation fileInfo = new FileInformation();
        File[] files = fileInfo.getFileInfo(arguments.dirName);


        if (files == null || files.length == 0){
            System.out.println("Files not found!");
        } else {
            for (int i = 0; i < files.length; i++) {
                System.out.println("Name: " + files[i].getName() + ", size(bytes): " + files[i].length());
            }
        }
    }
}
