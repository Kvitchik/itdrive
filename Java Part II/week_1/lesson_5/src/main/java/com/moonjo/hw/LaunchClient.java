package com.moonjo.hw;

import java.util.Scanner;

public class LaunchClient {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        pro.moonjo.threads.sockets.itdrive_multithreading.SocketClient socketClient = new pro.moonjo.threads.sockets.itdrive_multithreading.SocketClient("127.0.0.1",7777);

        while(true) {
            String message = scanner.nextLine();
            socketClient.sendMessage(message);
        }
    }
}
