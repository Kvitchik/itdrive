package com.moonjo.hw;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Bridge extends Thread {
    List<Bridge> threads = new ArrayList<>();
    PrintWriter writer;

    private Socket socketClient;
    public Bridge(Socket socketClient) {
        this.socketClient = socketClient;
    }
    @Override
    public void run() {
        try {
            writer = new PrintWriter(socketClient.getOutputStream(), true);
            threads.add(this);
            while (true) {
                InputStream clientInputStream = socketClient.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientInputStream));
                String inputLine = null;
                inputLine = bufferedReader.readLine();
                while (inputLine != null) {
                    writer.println(inputLine);
                    for (Bridge bridge : threads) {
                        bridge.sendMessageToAllThreads(inputLine);
                    }
                    inputLine = bufferedReader.readLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void sendMessageToAllThreads(String message) throws IOException {
        writer = new PrintWriter(socketClient.getOutputStream(), true);
        writer.println(message);
    }
}
