package com.moonjo.hw;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServerSocket {
//    List<Bridge> connections = new ArrayList<>();
    public void start(int port) {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                Socket socketClient = serverSocket.accept();
                System.out.println("Новый клиент подключен!");
                Bridge newBridge = new Bridge(socketClient);
//                connections.add(newBridge);
                newBridge.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
