package com.moonjo.directoryinfo;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters(separators="=")
class Arguments {

    @Parameter(names = {"--dirName"})
    public String dirName;

    @Parameter(names = { "--names"})
    public List<String> names;
}
