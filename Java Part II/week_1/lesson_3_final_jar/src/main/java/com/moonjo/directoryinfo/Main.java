package com.moonjo.directoryinfo;

import com.beust.jcommander.JCommander;
import com.moonjo.directoryinfo.utils.FileInformation;
import pro.moonjo.hw.PrintFileInfo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);
        FileInformation fileInfo = new FileInformation();
        PrintFileInfo printFileInfo = new PrintFileInfo();
        Map<String, File[]> result = printFileInfo.print(fileInfo, arguments.names);

        for (Map.Entry<String, File[]> entry : result.entrySet()) {
            for (File file : entry.getValue()) {
                System.out.println("Folder: " + entry.getKey() + " | Name: " +
                        file.getName() +
                        " | Size: " + file.length());
            }
            System.out.println("-----------------------------------------------------------------");
        }
    }
}
