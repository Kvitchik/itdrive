package com.moonjo.hw;

public class Main {
    public static void main(String[] args) {
        ThreadPool pool = new ThreadPool(5);
        pool.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " - Egg");
            }
        });
        pool.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " - Hen");
            }
        });
        pool.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " - Human");
            }
        });
        pool.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " - Car");
            }
        });
        pool.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " - Text");
            }
        });
    }
}
