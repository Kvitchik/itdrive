package com.moonjo.directoryinfo.utils;

import java.io.File;

public class FileInformation {
    public File[] getFileInfo(String directoryName) {
        File dir = new File(directoryName);
        if (dir.exists()) {
            return dir.listFiles();
        } else {
            return null;
        }
    }
}
