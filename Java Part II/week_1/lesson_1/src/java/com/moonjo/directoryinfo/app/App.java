package com.moonjo.directoryinfo.app;

import com.moonjo.directoryinfo.utils.FileInformation;
import com.beust.jcommander.JCommander;
import java.io.File;

public class App {
    public static void main(String[] args) {
        Arguments arguments = new Arguments();

        JCommander.newBuilder()
            .addObject(arguments)
            .build()
            .parse(args);

        FileInformation fileInfo = new FileInformation();
        File[] files = fileInfo.getFileInfo(arguments.dirName);

        if (files == null){
            System.out.println("Files not found!");
        } else {
            for (int i = 0; i < files.length; i++) {
                System.out.println("Name: " + files[i].getName() + ", size(bytes): " + files[i].length());
            }
        }
    }
}
