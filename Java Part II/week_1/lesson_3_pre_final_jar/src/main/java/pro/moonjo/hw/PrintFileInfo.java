package pro.moonjo.hw;

import com.moonjo.directoryinfo.utils.FileInformation;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrintFileInfo {
    public Map<String, File[]> print(FileInformation fileInfo,  List<String> folders) throws InterruptedException {
        int index = 0;
        Map<String, File[]> result = new HashMap<>();
        Thread[] threads = new Thread[folders.size()];
        for (String file : folders) {
            Thread thread = new Thread(() -> {
                result.put(file, fileInfo.getFileInfo(file));
            });
            thread.start();
            threads[index++] = thread;
        }
        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return result;
//        for (Map.Entry<String, File[]> entry : result.entrySet()) {
//            for (File file : entry.getValue()) {
//                System.out.println("Folder: " + entry.getKey() + " | Name: " +
//                        file.getName() +
//                        " | Size: " + file.length());
//            }
//            System.out.println("-----------------------------------------------------------------");
//        }
    }
}
