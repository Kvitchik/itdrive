package pro.moonjo.hw;

import com.beust.jcommander.JCommander;
import com.moonjo.directoryinfo.utils.FileInformation;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);
        FileInformation fileInfo = new FileInformation();
        PrintFileInfo printFileInfo = new PrintFileInfo();
        printFileInfo.print(fileInfo,arguments.names);
    }
}
