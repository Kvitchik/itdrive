package pro.moonjo.hw;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters(separators = "=")
class Arguments {

    @Parameter(names = {"--dirName"})
    public String dirName;

    @Parameter(names = {"--names"}, variableArity = true)
    public List<String> names;
}
